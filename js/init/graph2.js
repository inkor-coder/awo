var data2;

data2 = [
  {
    'key': 'One',
    'values': [[1, 0], [2, .5], [3, 1], [4, .5], [5, 0], [6, 0], [7, 0], [8, 0]],
    'color': '#01b9c2'
  }, {
    'key': 'Two',
    'values': [[1, 0], [2, 0], [3, 0], [4, 0], [5, 1.2], [6, .6], [7, 0], [8, 0]],
    'color': '#f7954e'
  }
];

nv.addGraph(function() {
  var chart;
  chart = nv.models.stackedAreaChart().x(function(d) {
    return d[0];
  }).y(function(d) {
    return d[1];
  }).useInteractiveGuideline(false).rightAlignYAxis(false).showControls(false).clipEdge(false);
  d3.select('#chart2 svg:first-child').datum(data2).call(chart);
  nv.utils.windowResize(chart.update);
  return chart;
});
