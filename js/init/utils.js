var isNumberKey, makeid, windowHeight, windowWidth;

windowWidth = function() {
  var w;
  w = 0;
  if (window.innerWidth > window.outerWidth) {
    w = window.outerWidth;
  } else {
    w = window.innerWidth;
  }
  return Math.max(w, $(window).width());
};

windowHeight = function() {
  var w;
  w = 0;
  if (window.innerHeight > window.outerHeight) {
    w = window.outerHeight;
  } else {
    w = window.innerHeight;
  }
  return Math.max(w, $(window).height());
};

isNumberKey = function(evt) {
  var charCode;
  charCode = evt.which ? evt.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;
};

makeid = function() {
  var i, j, possible, text;
  text = "";
  possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (i = j = 0; j < 5; i = ++j) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};


/*
	info block
 */

$(function() {
  var bindStateChanger, cloneFrom;
  cloneFrom = function(jqueryObject) {
    var cloned;
    cloned = jqueryObject.clone();
    cloned.find('input, select, textarea').each(function(i, input) {
      var newId;
      newId = makeid();
      $("[for='" + ($(input).attr('id')) + "']", cloned).attr('for', newId);
      $(input).attr('id', newId).val('');
    });
    return cloned;
  };
  bindStateChanger = function(context) {
    $('[data-changestate]', context).click(function(e) {
      var elementSelector, elementState;
      elementSelector = $(this).data('changestate').split(',')[0];
      elementState = $(this).data('changestate').split(',')[1];
      if ($(this).is(':checked')) {
        $(elementSelector, context).attr(elementState, '');
        $(elementSelector, context).select2().prop('disabled', true);
      } else {
        $(elementSelector, context).removeAttr(elementState);
        $(elementSelector, context).select2().prop('disabled', false);
      }
    });
    if ($('[data-changestate]', context).is(':checked')) {
      $('[data-changestate]', context).click();
    }
  };
  $('select').select2();
  $('.info-block').each(function(i, infoBlock) {
    $(infoBlock).on('click', '.add-new-btn', function(e) {
      var newRow;
      e.preventDefault();
      $('select').select2('destroy');
      newRow = cloneFrom($('.block', infoBlock).eq(0));
      newRow.insertAfter($('.block', infoBlock).eq(-1));
      bindStateChanger(newRow);
      $('select').select2();
    });
    $(infoBlock).on('click', '.remove-btn', function(e) {
      e.preventDefault();
      if ($(this).parents('.info-block').find('.block').length === 1) {
        return;
      }
      $(this).parents('.block').remove();
    });
    bindStateChanger($('.block', infoBlock).eq(0));
  });
});
