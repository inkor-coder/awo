nv.addGraph(function() {
  var chart;
  chart = nv.models.pieChart().x(function(d) {
    return d.label;
  }).y(function(d) {
    return d.value;
  }).showLabels(false).labelThreshold(.1).labelType('percent').donut(true).donutRatio(0.7);
  d3.select('#chart1 svg').datum([
    {
      'label': 'One',
      'value': 80,
      'color': '#f7954e'
    }, {
      'label': 'Two',
      'value': 20,
      'color': '#ebeded'
    }
  ]).transition().duration(350).call(chart);
  nv.utils.windowResize(chart.update);
  return chart;
});
