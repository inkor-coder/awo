nv.addGraph(function() {
  var chart;
  chart = nv.models.pieChart().x(function(d) {
    return d.label;
  }).y(function(d) {
    return d.value;
  }).showLabels(true);
  d3.select('#chart3 svg').datum([
    {
      'label': '50%',
      'value': 50,
      'color': '#9bc05b'
    }, {
      'label': '40%',
      'value': 40,
      'color': '#ef7063'
    }, {
      'label': '100%',
      'value': 100,
      'color': '#ebeded'
    }
  ]).transition().duration(350).call(chart);
  nv.utils.windowResize(chart.update);
  return chart;
});
