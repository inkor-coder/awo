$(document).ready(function() {
  var checkVerticalImages, toggleMainMenu, waitForFinalEvent;
  waitForFinalEvent = (function() {
    var timers;
    timers = {};
    return function(callback, ms, uniqueId) {
      if (!uniqueId) {
        uniqueId = 'Don\'t call this twice without a uniqueId';
      }
      if (timers[uniqueId]) {
        clearTimeout(timers[uniqueId]);
      }
      timers[uniqueId] = setTimeout(callback, ms);
    };
  })();
  checkVerticalImages = function() {
    $('.main-slider .image-cover img').each(function(i, img) {
      $(img).parent().removeClass('vertical');
      if ($(img).width() <= $(document).width()) {
        $(img).parent().addClass('vertical');
      }
    });
  };
  $('.main-slider').owlCarousel({
    singleItem: true,
    navigation: false,
    pagination: true,
    autoPlay: 5000,
    stopOnHover: true,
    transitionStyle: 'fade',
    mouseDrag: false,
    touchDrag: false,
    afterInit: function() {
      checkVerticalImages();
    }
  });
  $('[data-scrollto]').click(function(e) {
    var offset;
    e.preventDefault();
    offset = windowWidth() <= 768 ? $('.main-header').height() : 0;
    $('html, body').animate({
      scrollTop: $($(this).attr('data-scrollto')).offset().top - offset
    }, 1000, 'easeInOutQuart');
  });
  $('.testimonials').owlCarousel({
    singleItem: true,
    navigation: false,
    pagination: true,
    autoPlay: 5000,
    stopOnHover: true,
    mouseDrag: true,
    touchDrag: true,
    afterInit: function() {
      checkVerticalImages();
    }
  });
  (toggleMainMenu = function() {
    if ($('.main-nav-toggle').hasClass('active')) {
      $('.main-nav').addClass('active');
    } else {
      $('.main-nav').removeClass('active');
    }
  })();
  $('.main-nav-toggle').click(function(e) {
    e.preventDefault();
    $(this).toggleClass('active');
    toggleMainMenu();
  });
  $('body').click(function(e) {
    if ($(e.target).closest('.main-nav-toggle, .main-nav').length === 0) {
      $('.main-nav-toggle').removeClass('active');
      toggleMainMenu();
    }
  });
  $(window).scroll(function(e) {
    var top;
    top = $(document).scrollTop();
    if (top > windowHeight() / 2) {
      $('.main-header').addClass('white');
    } else {
      $('.main-header').removeClass('white');
    }
  });
  $(window).resize(function() {
    waitForFinalEvent((function() {
      checkVerticalImages();
    }), 200, '');
  });
});
